#include "character.hpp"
#ifndef WARRIOR_H
#define WARRIOR_H

class Warrior: public Character{
    protected:
    std::string name;   //name of character given by user
    int char_class;     //1 for warrior, 2 for mage, 3 for ranger, 4 for rogue
    int body;           //hitpoints
    int knowledge;      //intelligence
    int speed;          //dexterixty
    int luck;           //how lucky the char is, determined by random number
    int level;          //level
    int damage_melee;   //damage melee
    int damage_range;   //damage range
    int spell_power;    //damage magic
    int armour;         //what armour, links to armour class
    int weapon;         //weapon, links to weapon class
    int accessories;    //accessories, links to accessories class
    public:
    Warrior(std::string name, int char_class, int level, int armour, int weapon, int accessories);
    void setBasestats();
    void setDamagestats();
    int getBasestats();
    int getDamagestats();
};
#endif