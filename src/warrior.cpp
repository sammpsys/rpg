#include "character.hpp"
#include "warrior.hpp"

//Warrior::Warrior(){
      //constructor
    Warrior::Warrior(std::string name, int char_class, int level, int armour, int weapon, int accessories){
        this->name=name;
        this->char_class=char_class;
        this->level=level;
        this->armour=armour;
        this->weapon=weapon;
        this->accessories;

    }
    void Warrior::setBasestats(){
        body=5+level;       //high for warrior
        speed=3+level;
        knowledge=1+level;
    }

    void Warrior::setDamagestats(){
        if (weapon==1){         //sword
            damage_melee=5;   //damage melee
        }
        else{
            damage_melee=2;
        }
        damage_range=1;   //damage range
        spell_power=2; 
    }
    int Warrior::getBasestats(){
        return body, speed, knowledge;
    }
    int Warrior::getDamagestats(){
        return damage_melee, damage_range, spell_power;
    }
//}