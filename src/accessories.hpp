#ifndef ACCESSORIES_H
#define ACCESSORIES_H
#include <string>

class Item{
    protected:
        int item;
        std::string itemname;
    public:
        Item::Item(int item);
        void setItemname(int number);
};

#endif