#include <iostream>
#include <string>
#include "character.hpp"
#include "mage.hpp"

          //constructor
    Mage::Mage(std::string name, int char_class, int level, int armour, int weapon, int accessories){
        this->name=name;
        this->char_class=char_class;
        this->level=level;
        this->armour=armour;
        this->weapon=weapon;
        this->accessories;

    }
    void Mage::setBasestats(){
        body=1+level;       
        speed=2+level;
        knowledge=5+level;
    }

    void Mage::setDamagestats(){
        if (weapon==2){         //staff
            spell_power=5;   //damage mage
        }
        else{
            spell_power=2;
        }
        damage_range=1;   //damage range
        damage_melee=2; 
    }
    int Mage::getBasestats(){
        return body, speed, knowledge;
    }
    int Mage::getDamagestats(){
        return damage_melee, damage_range, spell_power;
    }

