#ifndef WEAPONS_H
#define WEAPONS_H
#include <string>

class Armour{
    protected:
        int armour;
        std::string armourname;
        int boost;
    public:
        Armour(int armour);
        void setArmourname(int number);
        int getboost(int number);
};

#endif