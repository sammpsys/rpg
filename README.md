## NAME
Rpg character generator

## DESCRIPTION
Purpose is to generate a random character based upon user input. This project is incomplete, however. Due to time constraints, there is no .main file. As such, it cannot be run. Additionally, the desired functionality has not been fully implemented. Because there is no main file, there are also no makefile instructions.

## DEVELOPERS
Sam Nassiri
