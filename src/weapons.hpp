#ifndef WEAPONS_H
#define WEAPONS_H
#include <string>

class Weapons{
    protected:
        int weapon;
        std::string weaponname;
        int damage;
    public:
        Weapons(int weapon);
        void setWeaponname(int number);
        int getDamage(int number);
};

#endif