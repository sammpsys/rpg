#include "character.hpp"

Character::Character(std::string name, int char_class, int level,int armour, int weapon, int accessories){
    this->name=name;
    this->char_class=char_class;
    this->level=level;
    this->armour=armour;
    this->weapon=weapon;
    this->accessories=accessories;
}