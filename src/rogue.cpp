#include "character.hpp"
#include "rogue.hpp"

         //constructor
    Rogue::Rogue(std::string name, int char_class, int level, int armour, int weapon, int accessories){
        this->name=name;
        this->char_class=char_class;
        this->level=level;
        this->armour=armour;
        this->weapon=weapon;
        this->accessories;

    }
    void Rogue::setBasestats(){
        body=2+level;       
        speed=4+level;      //high for rogue
        knowledge=4+level;
    }

    void Rogue::setDamagestats(){
        if (weapon==4){     //dagger
            damage_melee=5;   //damage melee
        }
        else{
            damage_melee=2;
        }
        damage_range=3;   //damage range
        spell_power=0; 
    }
    int Rogue::getBasestats(){
        return body, speed, knowledge;
    }
    int Rogue::getDamagestats(){
        return damage_melee, damage_range, spell_power;
    }

