#include "character.hpp"
#include "ranger.hpp"

          //constructor
    Ranger::Ranger(std::string name, int char_class, int level, int armour, int weapon, int accessories){
        this->name=name;
        this->char_class=char_class;
        this->level=level;
        this->armour=armour;
        this->weapon=weapon;
        this->accessories;

    }
    void Ranger::setBasestats(){
        body=3+level;       
        speed=5+level;      //high for ranger
        knowledge=2+level;
    }

    void Ranger::setDamagestats(){
        if (weapon==3){         //bow
            damage_range=5;   //damage melee
        }
        else{
            damage_range=2;
        }
        damage_melee=3;   //damage range
        spell_power=2; 
    }
    int Ranger::getBasestats(){
        return body, speed, knowledge;
    }
    int Ranger::getDamagestats(){
        return damage_melee, damage_range, spell_power;
    }

